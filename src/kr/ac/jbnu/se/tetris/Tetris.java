package kr.ac.jbnu.se.tetris;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Tetris extends JFrame {

	JLabel statusbar;
	Board board;

	public Tetris() {
		this.setLayout(null);
		statusbar = new JLabel(" 0");
		add(statusbar);
		Board board = new Board(this);
		board.setBounds(250,50,300,600);
		add(board);
		board.start();

		//홀드피스 박스
		HoldBox holdBox = new HoldBox();
		holdBox.setBounds(600,0,300, 200);
		add(holdBox);

		//블록 미리보기 박스
		NextBox nextBox = new NextBox();
		nextBox.setBounds(60,0,300, 300);
		add(nextBox);

		//HeavyBlock 카운팅 박스
		RemainHeavyBlockBox remainHeavyBlockBox = new RemainHeavyBlockBox();
		remainHeavyBlockBox.setBounds(600, 200, 300, 300);
		add(remainHeavyBlockBox);

		setSize(800, 800);
		setTitle("Tetris");
		setDefaultCloseOperation(EXIT_ON_CLOSE);


	}

	public JLabel getStatusBar() {
		return statusbar;
	}
	public Board getGameBoard() { return board; }
	public void startGame() { board.start(); }



	public static void main(String[] args) {
		Tetris game = new Tetris();
		game.setLocationRelativeTo(null);
		game.setVisible(true);
		game.startGame();
	}
}